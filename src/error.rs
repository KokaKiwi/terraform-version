use crate::Operator;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, thiserror::Error, miette::Diagnostic, PartialEq, Eq)]
pub enum Error {
    #[error("No version filled in")]
    NoVersion,

    #[error("No version requirement filled in")]
    NoVersionRequirement,

    #[error("Operator `{0}` forbidden. Non-empty suffix is only allowed if `operator` is Exact of Different")]
    NotAllowedOperatorWithSuffix(Operator),

    #[error("Invalid operator in comparator : `{0}`")]
    InvalidOperator(String),

    #[error("Impossible to parse numeric identifier `{text}` : `{ni}` is not a number")]
    ImpossibleNumericIdentifierParsing {
        #[source]
        err: std::num::ParseIntError,
        text: String,
        ni: String,
    },
}
